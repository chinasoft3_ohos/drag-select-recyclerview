# Drag Select Recycler View

#### 项目介绍
- 项目名称：drag-select-recyclerview
- 所属系列：openharmony的第三方组件适配移植
- 功能：drag-select-recyclerview是一个简单的多选列表功能库
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 2.4.0

#### 效果演示
![效果演示](./art/sample.gif)

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，

 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:drag-select-recyclerview:2.4.1')
    ......  
 }
 ```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.在xml文件里引用MyListContainer作为列表容器
```xml
<com.afollestad.library.component.MyListContainer
                 ohos:id="$+id:list"
                 ohos:height="match_parent"
                 ohos:width="match_parent"
                 ohos:background_element="#FFFFFF"
                 ohos:layout_alignment="horizontal_center">
</com.afollestad.library.component.MyListContainer>
```

2.创建provider并实现DragSelectReceiver接口，重写其中的方法，并设置给listContainer
```java
public class GridProvider extends BaseItemProvider implements DragSelectReceiver {
    // 重写BaseItemProvider和DragSelectReceiver的方法
}
```

``` java
ListContainer listContainer = (MyListContainer) findComponentById(ResourceTable.Id_list);
GridProvider gridProvider = new GridProvider(this);
listContainer.setItemProvider(gridProvider);
```

3.调用下面代码创建DragSelectTouchListener，并添加至listContainer  
(注：在创建DragSelectTouchListener之前，请务必先给listContainer设置已实现DragSelectReceiver接口的provider，否则会抛出异常)
``` java
DragSelectTouchListener dragSelectTouchListener = DragSelectTouchListener.create(listContainer);
listContainer.addDragSelectTouchListener(dragSelectTouchListener);
```

4.可以设置item的选中模式，当前有两种模式，一种为范围选中(RANGE)，一种为划过路径选中（PATH)
``` java
dragSelectTouchListener.setMode(DragSelectTouchListener.Mode.PATH);
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 2.4.1
- 0.0.1-SNAPSHOT