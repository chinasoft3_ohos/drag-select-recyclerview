/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.afollestad.library.utils;

import java.util.List;

/**
 * description : 自定义工具类
 *
 * @since 2021-03-12
 */
public class MyUtils {
    /**
     * 检测list是否有元素
     *
     * @param list 待检测的list
     * @return true 表示该list为空集合没有元素； false 表示该list有元素
     */
    public static boolean isListNoElement(List list) {
        if (list == null || list.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
