package com.afollestad.library.callback;

/**
 * description :
 *
 * @since : 2021/6/16
 */
public interface DragSelectReceiver {
    /**
     * 设置位于position位置的item的选中状态
     *
     * @param position item 的位置
     * @param select   选中状态
     */
    void setItemSelect(int position, boolean select);

    /**
     * 设置item长按的监听
     *
     * @param listener item长按监听
     */
    void setOnItemLongClickListener(OnItemLongClickListener listener);

    /**
     * 切换位于position位置的item的选中状态
     *
     * @param position item的位置
     */
    void setItemSelect(int position);
}
