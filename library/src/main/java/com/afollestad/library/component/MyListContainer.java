/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.afollestad.library.component;

import com.afollestad.library.callback.DragSelectTouchListener;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * description : 自定义listContainer，重写onTouchEvent方法
 *
 * @since 2021-03-12
 */
public class MyListContainer extends ListContainer implements Component.TouchEventListener {
    private DragSelectTouchListener dragSelectTouchListener;

    /**
     * 初始化MyListContainer
     *
     * @param context context
     */
    public MyListContainer(Context context) {
        super(context);
        setTouchEventListener(this);
    }

    /**
     * 初始化MyListContainer
     *
     * @param context context
     * @param attrSet attrSet
     */
    public MyListContainer(Context context, AttrSet attrSet) {
        super(context, attrSet);
        setTouchEventListener(this);
    }

    /**
     * 初始化MyListContainer
     *
     * @param context   context
     * @param attrSet   attrSet
     * @param styleName styleName
     */
    public MyListContainer(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setTouchEventListener(this);
    }

    /**
     * 添加dragSelectTouchListener监听
     *
     * @param listener dragSelectTouchListener监听
     */
    public void addDragSelectTouchListener(DragSelectTouchListener listener) {
        this.dragSelectTouchListener = listener;
    }

    /**
     * onTouch 监听触发的回调
     *
     * @param component  当前component
     * @param touchEvent touchEvent
     * @return true 表示该控件拦截并消费此touch事件； false 表示该控件不拦截此touch事件
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (dragSelectTouchListener != null) {
            if (component instanceof ListContainer) {
                dragSelectTouchListener.onTouch((ListContainer) component, touchEvent);
            }
            return true;
        } else {
            return false;
        }
    }

}
