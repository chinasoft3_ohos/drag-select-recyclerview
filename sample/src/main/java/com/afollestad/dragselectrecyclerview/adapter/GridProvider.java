package com.afollestad.dragselectrecyclerview.adapter;

import com.afollestad.library.ResourceTable;
import com.afollestad.dragselectrecyclerview.bean.DragSelectItem;
import com.afollestad.library.callback.DragSelectReceiver;
import com.afollestad.library.callback.OnItemLongClickListener;
import com.afollestad.dragselectrecyclerview.callback.OnItemSelectChangeListener;
import com.afollestad.library.utils.PixelUtil;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * description : GridProvider
 *
 * @since 2021-03-12
 */
public class GridProvider extends BaseItemProvider implements DragSelectReceiver {
    private List<DragSelectItem> mDataList = new ArrayList<>();
    private final LayoutScatter layoutScatter;
    private OnItemLongClickListener listener;
    private boolean isMenuShow;
    private OnItemSelectChangeListener onItemSelectChangeListener;

    /**
     * 设置item长按的监听
     *
     * @param listener item长按监听
     */
    @Override
    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
        this.listener = listener;
    }

    /**
     * 设置item选中状态改变的监听
     *
     * @param onItemSelectChangeListener item选中状态改变的监听
     */
    public void setOnItemSelectChangeListener(OnItemSelectChangeListener onItemSelectChangeListener) {
        this.onItemSelectChangeListener = onItemSelectChangeListener;
    }

    static class ViewHolder {
        Text text;
    }

    /**
     * 设置Menu是否显示
     *
     * @param menuShow Menu是否显示
     */
    public void setMenuShow(boolean menuShow) {
        isMenuShow = menuShow;
    }

    /**
     * 初始化GridProvider
     *
     * @param context Context
     */
    public GridProvider(Context context) {
        this.layoutScatter = LayoutScatter.getInstance(context);
    }

    /**
     * 设置Provider的数据源
     *
     * @param list Provider的数据源
     */
    public void setDataList(List<DragSelectItem> list) {
        this.mDataList = list;
    }

    /**
     * 获取Provider的数据源
     *
     * @return Provider的数据源
     */
    public List<DragSelectItem> getDataList() {
        return mDataList;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder = null;
        Component tempComponent = component;
        if (tempComponent == null) {
            viewHolder = new ViewHolder();
            tempComponent = layoutScatter.parse(ResourceTable.Layout_grid_item_list, null, false);
            if (tempComponent.findComponentById(ResourceTable.Id_grid_text) instanceof Text) {
                viewHolder.text = (Text) tempComponent.findComponentById(ResourceTable.Id_grid_text);
            }
            tempComponent.setTag(viewHolder);
        } else {
            if (tempComponent.getTag() instanceof ViewHolder) {
                viewHolder = (ViewHolder) tempComponent.getTag();
            }
        }
        DragSelectItem itemData = mDataList.get(position);
        // 设置text的宽高
        int width = (PixelUtil.screenWidth() - (3 * 2) * 10) / 3;
        DependentLayout.LayoutConfig itemConfig = new DependentLayout.LayoutConfig(width, (int) (width * 1.4f));
        itemConfig.setMargins(10, 10, 10, 10);
        assert viewHolder != null;
        viewHolder.text.setLayoutConfig(itemConfig);
        viewHolder.text.setText(itemData.getName());
        // 设置前景
        ShapeElement foreground = new ShapeElement();
        if (itemData.isSelect()) {
            // 如果选中，增加一个半透明的前景
            foreground.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#B3000000")));
            viewHolder.text.setSelected(true);
        } else {
            foreground.setRgbColor(RgbColor.fromRgbaInt(Color.getIntColor("#00000000")));
            viewHolder.text.setSelected(false);
        }
        viewHolder.text.setForeground(foreground);
        // 设置背景
        ShapeElement background = new ShapeElement();
        background.setRgbColor(RgbColor.fromRgbaInt(Color.getIntColor(itemData.getColor())));
        viewHolder.text.setBackground(background);
        // 设置点击监听
        viewHolder.text.setClickedListener(component1 -> setItemSelect(position));
        // 设置长按监听
        viewHolder.text.setLongClickable(true);
        viewHolder.text.setLongClickedListener(component1 -> {
            // 如果菜单栏显示的话，不进行选中操作
            if (isMenuShow) {
                return;
            }
            if (listener != null) {
                listener.onItemLongClick(position);
            }
        });
        return tempComponent;
    }

    /**
     * 切换位于position位置的item的选中状态
     *
     * @param position item的位置
     */
    @Override
    public void setItemSelect(int position) {
        DragSelectItem item = mDataList.get(position);
        setItemSelect(position, !item.isSelect());
    }

    /**
     * 设置位于position位置的item的选中状态
     *
     * @param position item 的位置
     * @param select   选中状态
     */
    @Override
    public void setItemSelect(int position, boolean select) {
        // 如果菜单栏显示的话，不进行选中操作
        if (isMenuShow) {
            return;
        }
        DragSelectItem item = mDataList.get(position);
        item.setSelect(select);
        notifyDataSetItemChanged(position);
        if (onItemSelectChangeListener != null) {
            int selected = 0;
            // 遍历数据源，获取被选中item的个数
            for (DragSelectItem items : mDataList) {
                if (items.isSelect()) {
                    selected++;
                }
            }
            onItemSelectChangeListener.onItemSelectChangeListener(selected);
        }
    }
}
