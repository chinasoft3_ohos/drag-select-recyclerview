package com.afollestad.dragselectrecyclerview;

import com.afollestad.library.utils.PixelUtil;

import ohos.aafwk.ability.AbilityPackage;

/**
 * description : MyApplication
 *
 * @since 2021-03-12
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        PixelUtil.initContext(this);
    }
}
