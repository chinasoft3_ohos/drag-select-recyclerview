package com.afollestad.dragselectrecyclerview;

import com.afollestad.library.utils.MyUtils;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * description :
 *
 * @since : 2021/5/25
 */
public class MyUtilsTest {
    /**
     * testCase
     */
    @Test
    public void isListNoElement() {
        List<String> list = new ArrayList<>();
        list.add("a");
        boolean result = MyUtils.isListNoElement(list);
        Assert.assertFalse(result);
    }
}

